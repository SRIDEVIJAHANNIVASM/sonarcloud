FROM ubuntu:18.04

RUN apt-get update \
    && apt-get -y install curl \
    && apt-get -y install git \	
    && apt-get -y install wget \	
    && apt-get -y install net-tools \
    && apt-get -y install zip \
    && rm -rf /var/lib/apt/lists/*


RUN apt-get update && apt-get install -y lsb-release libgtk-3-0 libappindicator3-1 xdg-utils libxss1 libnss3 libnspr4 libasound2 libappindicator1 fonts-liberation libgconf2-4 libxss1 xdg-utils libasound2

	

RUN apt-get -y install openjdk-8-jdk \
    && java -version \
    && rm -rf /var/lib/apt/lists/*

RUN curl -sL https://deb.nodesource.com/setup_10.x | bash - && apt-get -y install nodejs \
    && node -v

